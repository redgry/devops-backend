# DevOps Labs

## Полезные ссылки
[Журнал](https://docs.google.com/spreadsheets/d/1RWxsZiQceDTo1sxO2_sOlxm6dnQTc7I78-0qsFFcBXQ/edit?usp=sharing)  
[Исходник лабы](https://github.com/RedGry/SOA/tree/lab2)

## Лабораторная работа №1
Задачи:
1. Изучить основные принципы DevOps и его влияние на процесс разработки ПО.
2. Установить и настроить необходимое ПО для совместной работы (Git).
3. Создать простое клиент-серверное приложение (согласовывается с преподавателем) и разместить его в системе контроля версий ([GitLab факультета](https://gitlab.se.ifmo.ru/), другое согласовать с преподавателем).
4. Настроить автоматическую сборку проекта с использованием инструментов непрерывной интеграции (CI) - **GitLab**.

### Как все это сделать?
1. Делаем так, чтобы проект у нас работал локально :)
2. Пушим все на GitLab репозиторий.
3. Создаем `.gitlab-ci.yml` файл и прописываем туда наш pipeline.
4. Видим, что у нас не грузится image т.к. у раннера по дефолту запрещены настройки загрузки внешних образов :(
5. Настраиваем свой GitLab Runner, на своем компе. Это делается в `Settings`->`CI\CD`->`Runners`->`Show Runner installation instructions`. Желательно все это через **docker** поднимать.
6. Потом ищем нужный нам образ для Maven + JDK 17 и коммитим.
7. Чудо, у нас все работает :)
