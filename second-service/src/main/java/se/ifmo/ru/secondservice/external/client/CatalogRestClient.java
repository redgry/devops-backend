package se.ifmo.ru.secondservice.external.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import se.ifmo.ru.secondservice.external.model.FlatListGetResponseDto;
import se.ifmo.ru.secondservice.external.model.RestClientFlat;

import java.util.List;


@Slf4j
@Component
public class CatalogRestClient {
    private final RestTemplate restTemplate;
    private final String serviceUrl = "http://localhost:40001/api/v1";

    @Autowired
    public CatalogRestClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public RestClientFlat getFlatById(long id){
        String url = serviceUrl + "/catalog/flats/" + id;

        try {
            return restTemplate.getForObject(url, RestClientFlat.class);
        } catch (HttpClientErrorException e) {
            log.error("Error fetching flat by ID: {}", id, e);
            return null;
        }

    }

    public List<RestClientFlat> getAllFlats(){
        String url = serviceUrl + "/catalog/flats";

        try {
            FlatListGetResponseDto flats = restTemplate.getForObject(url, FlatListGetResponseDto.class);

            return flats != null ? flats.getFlatGetResponseDtos() : null;
        } catch (HttpClientErrorException  e){
            log.error("Error fetching all flats", e);
            return null;
        }
    }
}
