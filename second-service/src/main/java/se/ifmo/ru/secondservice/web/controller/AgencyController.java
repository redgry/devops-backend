package se.ifmo.ru.secondservice.web.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.ifmo.ru.secondservice.mapper.FlatMapper;
import se.ifmo.ru.secondservice.service.api.AgencyService;


@RestController
@RequestMapping("/agency")
public class AgencyController {

    private final AgencyService agencyService;
    private final FlatMapper flatMapper;

    @Autowired
    public AgencyController(AgencyService agencyService, FlatMapper flatMapper){
        this.agencyService = agencyService;
        this.flatMapper = flatMapper;
    }

    @GetMapping("/find-with-balcony/{cheapest}/{balcony}")
    public ResponseEntity<?> getFlatWithBalcony(@PathVariable("cheapest") String cheapest, @PathVariable("balcony") String balcony) throws ClassNotFoundException {
        if ((cheapest.equals("true") || cheapest.equals("false")) && (balcony.equals("true") || balcony.equals("false"))) {

            return  ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(
                            flatMapper.toDto(agencyService.findFlatWithBalcony(Boolean.parseBoolean(cheapest), Boolean.parseBoolean(balcony)))
                    );
        }

        throw new IllegalArgumentException("Invalid parameters supplied");
    }

    @GetMapping("/get-most-expensive/{id1}/{id2}/{id3}")
    public long getFlatMostExpensive(@PathVariable("id1") long id1, @PathVariable("id2") long id2, @PathVariable("id3") long id3) throws ClassNotFoundException {
        return agencyService.getMostExpensiveFlat(id1, id2, id3);
    }

}
